export interface IListSchema {
  filters: string[];
  url: string;
}