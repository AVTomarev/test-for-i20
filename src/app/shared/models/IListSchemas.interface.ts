import { IListSchema } from './IListSchema.interface';

export interface IListSchemas {
  [listName: string]: IListSchema;
}