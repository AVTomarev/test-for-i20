import { IPropsAndTypes } from 'src/app/shared/models/validation/models';
import { containsValidProps } from 'src/app/shared/models/validation/containsValidProps.validator';
import { IPerson } from './IPerson.interface';
import { IStarship } from './IStarship.interface';
import { IFilm } from './iFilm.interface';

export interface ISWResponse {
  count: number;
  next: string | null;
  previous: string | null;
  results: Array<IPerson | IStarship | IFilm>;
}

export const isSWResponse = (value: any): value is ISWResponse => {
  if (typeof value !== 'object' || Array.isArray(value)) { return false; }

  const requiredProps: IPropsAndTypes = {
    count: [ { typeName: 'number' } ]
  };

  return containsValidProps(value, requiredProps);
}
