import { IPropsAndTypes } from 'src/app/shared/models/validation/models';
import { containsValidProps } from 'src/app/shared/models/validation/containsValidProps.validator';
import { isString } from 'src/app/shared/models/validation/simpleTypesValidators';

export interface IPerson {
  birth_year: string;
  created: Date;
  edited: Date;
  eye_color: string;
  films: string[];
  gender: string;
  hair_color: string;
  height: string;
  homeworld: string;
  mass: string;
  name: string;
  skin_color: string;
  species: string[];
  starships: string[];
  url: string;
  vehicles: string[];
}

export const isPerson = (value: any): value is IPerson => {
  if (typeof value !== 'object' || Array.isArray(value)) { return false; }

  const requiredProps: IPropsAndTypes = {
    birth_year: [ { typeName: 'string' } ],
    eye_color: [ { typeName: 'string' } ],
    films: [ { typeName: 'array', validateFunction: isString } ],
    gender: [ { typeName: 'string' } ],
    hair_color: [ { typeName: 'string' } ],
    height: [ { typeName: 'string' } ],
    homeworld: [ { typeName: 'string' } ],
    mass: [ { typeName: 'string' } ],
    name: [ { typeName: 'string' } ],
    skin_color: [ { typeName: 'string' } ],
    species: [ { typeName: 'array', validateFunction: isString } ],
    starships: [ { typeName: 'array', validateFunction: isString } ],
    url: [ { typeName: 'string' } ],
    vehicles: [ { typeName: 'array', validateFunction: isString } ]
  };

  return containsValidProps(value, requiredProps);
}
