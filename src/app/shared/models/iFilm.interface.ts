import { IPropsAndTypes } from 'src/app/shared/models/validation/models';
import { containsValidProps } from 'src/app/shared/models/validation/containsValidProps.validator';
import { isString } from 'src/app/shared/models/validation/simpleTypesValidators';

export interface IFilm {
  episode_id: number;
  title: string;
  url: string;
  created: Date;
  edited: Date;
  release_date: Date;
  characters: string[];
  director: string;
  opening_crawl: string;
  planets: string[];
  producer: string;
  species: string[];
  starships: string[];
  vehicles: string[];
}

export const isFilm = (value: any): value is IFilm => {
  if (typeof value !== 'object' || Array.isArray(value)) { return false; }

  const requiredProps: IPropsAndTypes = {
    episode_id: [ { typeName: 'number' } ],
    title: [ { typeName: 'string' } ],
    url: [ { typeName: 'string' } ],
    characters: [ { typeName: 'array', validateFunction: isString } ],
    director: [ { typeName: 'string' } ],
    opening_crawl: [ { typeName: 'string' } ],
    planets: [ { typeName: 'array', validateFunction: isString } ],
    producer: [ { typeName: 'string' } ],
    species: [ { typeName: 'array', validateFunction: isString } ],
    starships: [ { typeName: 'array', validateFunction: isString } ],
    vehicles: [ { typeName: 'array', validateFunction: isString } ],
  };

  return containsValidProps(value, requiredProps);
}