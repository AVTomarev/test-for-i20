type TExpectedType = 'string' | 'number' | 'boolean' | 'object' | 'array';

export interface IExpectedType {
  typeName: TExpectedType;
  validateFunction?: (value: any) => boolean;
}

export interface IPropsAndTypes {
  [propName: string]: IExpectedType[];
}
