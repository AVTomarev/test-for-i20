import { isValidType } from './isValidType.validator';
import { IExpectedType } from './models';

export function checkExpectedTypesArray(
  value: any,
  typesArr: IExpectedType[]
): boolean {
  
  const result = typesArr.reduce((res: boolean, type: IExpectedType) => {
    if (!res) { return res; }
    
    const {
      typeName,
      validateFunction
    } = type;
    let isValid;

    if (Array.isArray(value) &&
        typeName === 'array' &&
        validateFunction
    ) {
      isValid = checkArrayOfValues(value, validateFunction);
    } else {
      if (validateFunction) {
        isValid = validateFunction(value);
      } else {
        isValid = isValidType(value, typeName);
      }
    }

    return isValid ? isValid : false;

  }, true);

  return result;
}

function checkArrayOfValues(
  arr: any[],
  validateFunction: (obj: any) => boolean
): boolean {
  
  const result = arr.reduce((res: boolean, item: any) => {
    if (!res) { return res; }

    const isValid = validateFunction(item);

    return isValid ? isValid : false;
  }, true);

  return result
}