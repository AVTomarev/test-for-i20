export const isValidType = (value: any, type: string): boolean => {
  return typeof value === type;
}
