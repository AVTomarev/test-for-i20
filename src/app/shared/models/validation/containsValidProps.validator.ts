 import { checkExpectedTypesArray } from './checkExpectedTypesArray.validator';
import { IPropsAndTypes } from './models';

export const containsValidProps = (object: any, props: IPropsAndTypes): boolean => {
  
  let propKeys = Object.keys(props);

  const result = propKeys.reduce((res: boolean, key: string) => {
    if (!res) { return res; }

    const isValid = checkExpectedTypesArray(object[key], props[key]);

    return isValid ? isValid : false;
  }, true);

  return result;
}