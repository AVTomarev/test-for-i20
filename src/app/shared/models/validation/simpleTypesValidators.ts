import { isValidType } from './isValidType.validator';

export const isString = (item: any): boolean => {
  return isValidType(item, 'string');
}

export const isNumber = (item: any): boolean => {
  return isValidType(item, 'number');
}

export const isBoolean = (item: any): boolean => {
  return isValidType(item, 'boolean');
}