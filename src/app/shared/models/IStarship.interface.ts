import { IPropsAndTypes } from 'src/app/shared/models/validation/models';
import { containsValidProps } from 'src/app/shared/models/validation/containsValidProps.validator';
import { isString } from 'src/app/shared/models/validation/simpleTypesValidators';

export interface IStarship {
  MGLT: string;
  cargo_capacity: string;
  consumables: string;
  cost_in_credits: string;
  created: Date;
  crew: string;
  edited: Date;
  films: string[];
  hyperdrive_rating: string;
  length: string;
  manufacturer: string;
  max_atmosphering_speed: string;
  model: string;
  name: string;
  passengers: string;
  pilots: string[]
  starship_class: string;
  url: string;
}

export const isStarship = (value: any): value is IStarship => {
  if (typeof value !== 'object' || Array.isArray(value)) { return false; }

  const requiredProps: IPropsAndTypes = {
    MGLT: [ { typeName: 'string' } ],
    cargo_capacity: [ { typeName: 'string' } ],
    consumables: [ { typeName: 'string' } ],
    cost_in_credits: [ { typeName: 'string' } ],
    crew: [ { typeName: 'string' } ],
    films: [ { typeName: 'array', validateFunction: isString } ],
    hyperdrive_rating: [ { typeName: 'string' } ],
    length: [ { typeName: 'string' } ],
    manufacturer: [ { typeName: 'string' } ],
    max_atmosphering_speed: [ { typeName: 'string' } ],
    model: [ { typeName: 'string' } ],
    name: [ { typeName: 'string' } ],
    passengers: [ { typeName: 'string' } ],
    pilots: [ { typeName: 'array', validateFunction: isString } ],
    starship_class: [ { typeName: 'string' } ],
    url: [ { typeName: 'string' } ]
  };

  return containsValidProps(value, requiredProps);
}
