import { IListSchemas } from 'src/app/shared/models/IListSchemas.interface';

export const Schemas: IListSchemas = {
	'films': {
		filters: ['title', 'release_date'],
		url: 'https://swapi.co/api/films/'
	},
	'people': {
		filters: ['name', 'gender'],
		url: 'https://swapi.co/api/people/'
	},
	'starships': {
		filters: ['name', 'starship_class'],
		url: 'https://swapi.co/api/starships/'
	}
};
