import { TestBed } from '@angular/core/testing';

import { SWValidationService } from './sw-validation.service';

describe('SWValidationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SWValidationService = TestBed.get(SWValidationService);
    expect(service).toBeTruthy();
  });
});
