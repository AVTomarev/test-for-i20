import { Injectable } from '@angular/core';

import { isPerson } from 'src/app/shared/models/IPerson.interface';
import { isFilm } from 'src/app/shared/models/iFilm.interface';
import { isStarship } from 'src/app/shared/models/IStarship.interface';
import { isSWResponse } from '../../models/ISWResponse.interface';

@Injectable({
  providedIn: 'root'
})
export class SWValidationService {

  constructor() { }

  public validateItem(item: any): boolean {
    if (
      !isPerson(item) && 
      !isFilm(item) && 
      !isStarship(item)
    ) {
      return false;
    } 

    return true;
  }

  public validateList(list: any): boolean {
    return isSWResponse(list);
  }
}
