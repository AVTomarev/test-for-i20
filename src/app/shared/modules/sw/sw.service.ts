import { Injectable } from '@angular/core';

import { SWFetchService } from './sw-fetch.service';
import { SWValidationService } from './sw-validation.service';

import { IFilm } from 'src/app/shared/models/iFilm.interface';
import { ISWResponse } from 'src/app/shared/models/ISWResponse.interface';
import { IPerson } from 'src/app/shared/models/IPerson.interface';
import { IStarship } from 'src/app/shared/models/IStarship.interface';

@Injectable({
  providedIn: 'root'
})
export class SWService {

  constructor(
    private swfetch: SWFetchService,
    private swvalidation: SWValidationService
  ) { }

  public async getList(url: string): Promise<ISWResponse | undefined> {
    const data = await this.swfetch
      .getData(url)
      .then(data => data);

    const isValid = this.swvalidation.validateList(data);

    return isValid ? data : undefined;
  }

  public async getItem(url: string): Promise<IFilm | IStarship | IPerson | undefined> {
    const data = await this.swfetch
      .getData(url)
      .then(data => data);

    const isValid = this.swvalidation.validateItem(data);

    return isValid ? data : undefined;
  }
}
