import { TestBed } from '@angular/core/testing';

import { SWFetchService } from './sw-fetch.service';

describe('SWFetchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SWFetchService = TestBed.get(SWFetchService);
    expect(service).toBeTruthy();
  });
});
