import { NgModule } from '@angular/core';

import { SWService } from './sw.service';
import { SWFetchService } from './sw-fetch.service';
import { SWValidationService } from './sw-validation.service';

@NgModule({
  providers: [
    SWService,
    SWFetchService,
    SWValidationService
  ]
})
export class SWModule { }
