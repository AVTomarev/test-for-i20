import { TestBed } from '@angular/core/testing';

import { SWService } from './sw.service';

describe('SWService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SWService = TestBed.get(SWService);
    expect(service).toBeTruthy();
  });
});
