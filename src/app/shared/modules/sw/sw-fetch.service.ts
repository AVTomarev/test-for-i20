import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SWFetchService {

  constructor(
    private httpClient: HttpClient
  ) { }

  public getData(url: string): Promise<any> {
    return this.httpClient.get(url).toPromise();
  }
}
