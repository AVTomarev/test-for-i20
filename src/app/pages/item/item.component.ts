import { Component, OnInit, DoCheck } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { IStarship } from 'src/app/shared/models/IStarship.interface';
import { IPerson } from 'src/app/shared/models/IPerson.interface';
import { IFilm } from 'src/app/shared/models/iFilm.interface';
import { SWService } from 'src/app/shared/modules/sw/sw.service';

interface IFilteredItem {
  [propName: string]: any;
}

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit, DoCheck {
  private item: IFilm | IStarship | IPerson;

  public itemToShow: IFilteredItem;

  public searchInput: string = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private swservice: SWService
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.getItem(params.url);
    });
  }

  ngDoCheck() {
    this.filterProps();
  }

  private filterProps(): void {
    if (this.searchInput === '') {
      this.itemToShow = this.item;
      return;
    }
    const uglyfiedStr = this.uglifyString(this.searchInput);

    const filteredKeys = Object
      .keys(this.item)
      .filter(key => {
        const uglifyedKey = this.uglifyString(key);

        return uglifyedKey.includes(uglyfiedStr);
      });

    const filteredItem = filteredKeys
      .reduce((res: IFilteredItem, key: string) => {
        res[key] = this.item[key];
        return res;
      }, {});

    this.itemToShow = filteredItem;
  }

  private async getItem(itemUrl: string): Promise<void> {
    this.item = await this.swservice.getItem(itemUrl);
    this.itemToShow = this.item;
  }

  private uglifyString(str: string): string {
    const spaceToUnderscore = str.replace(' ', '_');
    
    const toLowerCase = spaceToUnderscore.toLowerCase();

    return toLowerCase;
  }

  public goBack(): void {
    this.location.back();
  }
}
