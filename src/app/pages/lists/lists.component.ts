import { Component, OnInit, DoCheck } from '@angular/core';

import { SWService } from 'src/app/shared/modules/sw/sw.service';

import { Schemas } from 'src/app/shared/mock/list-schemas';

import { IStarship, isStarship } from 'src/app/shared/models/IStarship.interface';
import { IPerson, isPerson } from 'src/app/shared/models/IPerson.interface';
import { IFilm, isFilm } from 'src/app/shared/models/iFilm.interface';
import { ISWResponse } from 'src/app/shared/models/ISWResponse.interface';
import { IListSchemas } from 'src/app/shared/models/IListSchemas.interface';

interface IFilteringProps {
  propName: string;
  value: string;
}

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.scss']
})
export class ListsComponent implements OnInit, DoCheck {
  public data?: ISWResponse;

  private list?: Array<IFilm | IPerson | IStarship>;

  public listToShow: Array<IFilm | IPerson | IStarship> = [];

  public currentListType: string = 'starships';

  readonly schemas: IListSchemas;

  public first: string = '';

  public second: string = '';

  constructor(
    private swservice: SWService
  ) {
    this.schemas = Schemas;
  }

  ngOnInit() {
    this.getList(this.schemas.starships.url, 'starships');
  }

  ngDoCheck() {
    if (!this.list) { return; }

    if (this.first === '' && this.second === '') {
      this.listToShow = this.list;
    } else {
      this.listToShow = this.filterList(this.list);
    }
  }

  private filterList(
    list: Array<IFilm | IPerson | IStarship>
  ): Array<IFilm | IPerson | IStarship> {

    const result = list
      .filter((item) => {
        const filterProps: IFilteringProps[] = [{
          propName: this.schemas[this.currentListType].filters[0], 
          value: this.first
        }, {
          propName: this.schemas[this.currentListType].filters[1], 
          value: this.second
        }]

        switch(true) {
          case (isPerson(item) &&
                this.currentListType === 'people' &&
                this.isValidProps(item, filterProps)):

          case (isStarship(item) &&
                this.currentListType === 'starships' &&
                this.isValidProps(item, filterProps)):

          case (isFilm(item) &&
                this.currentListType === 'films' &&
                this.isValidProps(item, filterProps)):
            return true;

          default:
            return false;
        }
      });

    return result;
  }

  private isValidProps(
    item: IFilm | IPerson | IStarship,
    props: IFilteringProps[]
  ): boolean {

    const isValid = props.reduce((res: boolean, prop) => {
      if (!res) { return; }

      return item[prop.propName] &&
        (item[prop.propName] + '').toLowerCase().includes(prop.value.toLowerCase());
    }, true);

    return isValid;
  }

  private clearData(str: string): void {
    this.data  = undefined;
    this.list = undefined;

    if (str !== this.currentListType) {
      this.first = '';
      this.second = '';
    }
  }

  private setData(data: ISWResponse): void {
    this.data = data;
    this.list = data.results;
  }

  public async getList(
    str: string, 
    listType: string = this.currentListType
  ): Promise<void> {
    this.clearData(listType);

    this.currentListType = listType;

    const newData = await this.swservice.getList(str);
    this.setData(newData);
  }
}
