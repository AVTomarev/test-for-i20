import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemComponent } from './pages/item/item.component';
import { ListsComponent } from './pages/lists/lists.component';


const routes: Routes = [
  { path: '', component: ListsComponent },
  { path: 'item-info', component: ItemComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
