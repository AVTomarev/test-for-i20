import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { SWModule } from './shared/modules/sw/sw.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ItemComponent } from './pages/item/item.component';
import { ListsComponent } from './pages/lists/lists.component';

import { FirstCharToUpperCasePipe } from './shared/pipes/first-char-to-upper-case.pipe';
import { RemoveUnderscorePipe } from './shared/pipes/remove-underscore.pipe';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ItemComponent,
    ListsComponent,
    FirstCharToUpperCasePipe,
    RemoveUnderscorePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    SWModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
